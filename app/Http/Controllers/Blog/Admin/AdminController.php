<?php
/**
 * Created by PhpStorm.
 * User: Slobodian
 * Date: 23.11.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Blog\Admin;


class AdminController extends BaseController
{
    public function index()
    {
        return view('blog.admin.index');
    }
}