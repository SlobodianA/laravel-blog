<?php

namespace App\Http\Controllers\Blog;

use App\Repositories\BlogCategoryRepository;
use App\Repositories\BlogPostRepository;

class CategoryController extends BaseController
{
    /**
     * @var BlogPostRepository
     */
    private $blogPostRepository;
    /**
     * @var BlogCategoryRepository
     */
    private $blogCategoryRepository;

    public function __construct()
    {
        $this->blogPostRepository = app(BlogPostRepository::class);
        $this->blogCategoryRepository = app(BlogCategoryRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->blogCategoryRepository->getAllWithPaginateAndPostQuantity(6);
        $postsPerCategory = $this->blogPostRepository->getPostsPerCategory();
        $postsPerAuthor = $this->blogPostRepository->getPostsPerAuthor();
        return view('blog.categories.index', compact('paginator', 'postsPerAuthor', 'postsPerCategory'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paginator = $this->blogPostRepository->getCategoryPostsWithPaginate($id, 5);
        $postsPerCategory = $this->blogPostRepository->getPostsPerCategory();
        $postsPerAuthor = $this->blogPostRepository->getPostsPerAuthor();

        return view('blog.categories.category', compact('paginator', 'postsPerAuthor', 'postsPerCategory'));
    }

}
