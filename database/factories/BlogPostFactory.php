<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BlogPost;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(BlogPost::class, function (Faker $faker) {

    $title = $faker->sentence(mt_rand(3,8), true);
    $txt = $faker->realText(mt_rand(1000, 4000));
    $isPublished = mt_rand(1, 5) > 1;
    $createdDate = $faker->dateTimeBetween('-3 months', '-2 day');

    $data = [
        'category_id'   => mt_rand(1, 11),
        'user_id'       => mt_rand(1, 2),
        'title'         => $title,
        'slug'          => Str::slug($title, '-'),
        'excerpt'       => $faker->text(mt_rand(40, 100)),
        'content_raw'   => $txt,
        'content_html'  => $txt,
        'is_published'  => $isPublished,
        'published_at'  => $isPublished ? $faker->dateTimeBetween('-2 months', '-1 day') : null,
        'created_at'    => $createdDate,
        'updated_at'    => $createdDate,
    ];

    return $data;

});
