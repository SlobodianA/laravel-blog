<div class="row justify-content-center align-bottom mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Top Categories</div>
            <ul class="list-group list-group-flush">
                @foreach($postsPerCategory as $postPerCategory)
                <li class="list-group-item">
                    <a href="{{route('post.categories.show',$postPerCategory->category_id)}}">{{$postPerCategory->category->title}}:</a>
                    {{$postPerCategory->quantity}}
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<div class="row justify-content-center align-bottom">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Top Authors</div>

            <ul class="list-group list-group-flush">
                @foreach($postsPerAuthor as $postPerAuthor)
                <li class="list-group-item">
                    <a href="{{route('post.authors.show',$postPerAuthor->user_id)}}">{{$postPerAuthor->user->name}}:</a>
                    {{$postPerAuthor->quantity}}
                </li>
                @endforeach
            </ul>

        </div>
    </div>
</div>