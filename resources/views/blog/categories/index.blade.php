@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-light" href="{{route('post.blog.index')}}">&larr; All posts</a>
                </nav>
                <div class="row">
                    <div class="col-md-8 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-hover">
                                    <thead>
                                    <th>#</th>
                                    <th>Category</th>
                                    <th>Patent Category</th>
                                    <th>Number of articles</th>
                                    </thead>
                                    <tbody>
                                    @foreach($paginator as $category)
                                        @php /** @var \App\Models\BlogCategory $category */@endphp
                                        <tr>
                                            <td>{{$category->id}}</td>
                                            <td>
                                                <a href="{{route('post.categories.show', $category->id)}}">
                                                    {{$category->title}}
                                                </a>
                                            </td>
                                            <td @if (in_array($category->parent_id, [0,1])) style="color:#ccc;"@endif>
                                                {{$category->parentTitle}}
                                            </td>
                                            <td>{{$category->quantity}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        @include('blog.includes.toplist')
                    </div>
                </div>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    {{$paginator->links() }}
                </div>
            </div>
        @endif
    </div>
@endsection