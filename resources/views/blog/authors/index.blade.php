@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-light" href="{{route('post.blog.index')}}">&larr; All posts</a>
                </nav>
                <div class="row">
                    <div class="col-md-8 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-hover">
                                    <thead>
                                    <td>Author</td>
                                    <td>Number of articles</td>
                                    </thead>
                                    <tbody>
                                    @foreach($paginator as $author)
                                        @php /** @var \App\Models\BlogCategory $category */@endphp
                                        <tr>
                                            <td>
                                                <a href="{{route('post.authors.show', $author->user_id)}}">
                                                    {{$author->user->name}}
                                                </a>
                                            </td>
                                            <td>{{$author->quantity}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        @include('blog.includes.toplist')
                    </div>
                </div>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    {{$paginator->links() }}
                </div>
            </div>
        @endif
    </div>
@endsection