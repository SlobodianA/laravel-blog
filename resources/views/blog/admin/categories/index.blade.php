@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-light" href="{{route('blog.admin')}}">&larr; Back</a>
                    <a class="btn btn-primary" href="{{route('blog.admin.categories.create')}}">Add category</a>
                </nav>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                                <td>#</td>
                                <td>Category</td>
                                <td>Patent Category</td>
                            </thead>
                            <tbody>
                            @foreach($paginator as $category)
                                @php /** @var \App\Models\BlogCategory $category */@endphp
                                <tr>
                                    <td>{{$category->id}}</td>
                                    <td>
                                        <a href="{{route('blog.admin.categories.edit', $category->id)}}">
                                            {{$category->title}}
                                        </a>
                                    </td>
                                    <td @if (in_array($category->parent_id, [0,1])) style="color:#ccc;"@endif>
                                        {{$category->parentTitle}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {{$paginator->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection